# Découverte des fondamentaux de javascript 

Javascript est un langage qui s'éxécute côté client. 

<!-- A ne pas confondre avec Java qui est un autre langage qui n'a rien à voir !!  -->

Lien utilises :
- [La documentation MDN](https://developer.mozilla.org/fr/docs/Web/JavaScript)
- [W3 schools JS](https://www.w3schools.com/js/default.asp)
- [Javascript en 14 minutes](https://jgthms.com/javascript-in-14-minutes/)

## Les éléments de base du Javascript 
### Liaison entre le HTML et le javascript 
#### 1ère méthode 
Avec la balise ```<script>``` dans le ```<head>``` du HTML
Dans ce cas le javascript va se charger au moment de la balise va être interprété. 
Si il est nécessaire que le DOM HTML soit chargé avant le javascript, l faut ajouter l'attribut ```defer``` à la balise ```<script>```.

```html
<head>
    ...
<script src="path/file.js" type="text/javascript"></script>
<script src="path/file.js" type="text/javascript" defer></script>
```

#### 2ème méthide
Il suffit de placer les scripts juste avant la fermeture de la balise ```<body>```.

```html
<body>
    ...
<script src="path/file.js" type="text/javascript"></script>
</body>
```

### Les commentaires
Comme dans tous langages de programmation, il existe des commentaires en Javascript. On les distingue de 3 manières différentes :

```js
/**
 * Commentaire de documentation
 * 
 * @link https://fr.wikipedia.org/wiki/JSDoc Explication des commentaires de documentation
 * /
```

```js
/* Commentaire 
sur
plusieurs 
lignes
*/
```

```js
// Commentaire sur une ligne
```

Il est pratique d'utiliser les commentaires pour se laisser des explications sur des morceaux de code, des méthodes, des fonctions...
Il est également possible de les utiliser pour du debug, avec les tags TODO et FIX. 


```js
// TODO : Chose à faire à cet endroit 
```

```js
// FIX : Corriger ce problème ci
```

## Les éléments algorithmiques de Javascript 
### Les variables 

On déclare une variable avec le mot-reservé "var" ou "let" (on privilégiera la déclaration avec "let")
On affecte une valeur avec le caractère "="


```js
var variable1 = "valeur"
let variable2 = "autre valeur"
```
Le type de la variable sera définit dynamiquement en fonction de la valeur 

```js
let variableDeTypeString = "une chaine de caractères"
let variableDeTypeInterger = 12
let variableDeTypeBoolean = true
```

Note : le nom des variables est en camelCase. 

Il est possible de faire de la multiple déclaration en javascript. Il suffit de séparer les éléments par une virgule. 

```js
let a = 1, b = 2, 
    c = 3
```

### Les fonctions 
On déclare une fonction avec le mot-réservé "function"
C'est un bloc de code que l'on va pouvoir utiliser une fois qu'il est déclaré 
Une fonction peut posséder un ou plusieurs PARAMETRES. Si il y a plusieurs Paramétres, ils sont séparés par une virgule. 
Une fonction peut retourner un RESULTAT
Une fonction peut être utiliser pour FACTORISER du code 

```js
// Fonction sans paramétres, exemple :
function nomDeLaFonction{
    // code....
}
// Fonction avec un paramètre, exemple :
function nomDeLaFonction(paramSiBesoin) {
    // code....
}
// Fonction avec plusieurs paramètres, exemple :
function nomDeLaFonction(paramSiBesoin, paramSuivant) {
    // code....
}
// Fonction avec une valeur de retour, exemple :
function nomDeLaFonctionAvecRetour(paramSiBesoin, paramSuivant) {
    // code....
    return quelquesChose
}
```

Note : le nom des fonctions s'écrient en camelCase. 

Pour qu'une fonction est de l'utilité, il va falloir l'appeler. 

```js
// je déclare ma fonction :
function uneFonction() {
    // code....
}
// J'appelle ma fonction, ce qui va permettre d'éxécuter le code 
uneFonction()
```

### Les conditions
#### Structure conditionnelle n°1 : else/if
Une condition est "**déclaré**" avec le mot-réservé "if"
Entre les paranthèses on va y définir la condition à tester.
Puis on exécute le code si la condition est vrai. 
Si la condition est fausse on exécute le code qui est déclaré ia le mot-réservé "else".

```js
if (condition){
    // code à exécuter si ma condition est vraie 
}
else {
    // code à exécuter si ma condition est fausse
}
```

Si il faut utiliser plusieurs conditions, c'est possible de le faire également.
On utilise le mot-réservé "**else if**" pour définir d'autres conditions.
On mettra autant de "else if" que nécessaire.

```js
if (condition){
    // code à exécuter si ma condition est vraie 
}
else if (condition) {
    // code à exécuter si la condition du else if est vrai
}
else {
    // code à exécuter si la condition est fausse 
}

```

Lorsque l'on utilise une condition, le "**else**" n'est pas obligatoire.
Il n'est pas nécessaire de le mettre à chaque fois. 

#### Structure conditionnelle n°2 : switch/case
La différence par rapport au if else est qu'avec le switch/case, on test les différentes valeurs possibles est non si la condition est vraie dans tel ou tel cas. 
Le mot réservé pour le switch/case est "**switch**". 
Entre les paranthèses on va y définir la condition à tester.
Pour chaque possibilité on déclare avec le mot réservé "**case**" la valeur attendue à tester.
Pour sortir du switch/case on utilise le mot-réservé "**break**".

```js
switch(condition) {
    case valeur:
        // code à exécuter
        break
    case valeur:
        // code à exécuter 
        break
    default:
        // code à exécuter par défaut

}
```

#### Opérateurs utilisables pour les conditions
Les opérateurs permettent d'améliorer l'évaluation d'une expression de condition.
Il en existe différents types, en voici un extrait des principaux utilisées.

| Type d'opérateur                | Valeur |
| ------------------------------- | :----: |
| ET logique                      | &&     |
| OU logique                      | \|\|   |
| Négation                        | !      |
| Strictement supérieur à         | >      |
| Strictement inférieur à         | <      |
| Supérieur ou égale à            | >=     |
| Inférieur ou égale à            | <=     |
| Egalité de valeur               | ==     |
| Egalité de valeur ET de type    | ===    |
| Différent de valeur             | !=     |
| Différence de valeur et de type | !==    |

### Les boucles 
Les boucles permettront d'éxécuter plusieurs le même fragment de code, c'est ce qu'on appelera des **itérations**.

## La boucle While 
On utilise le mot réservé "**while**" pour déclarer une boucle while.
Entre parathéses la condition d'éxécution. 
Le while va se traduire par "**tant que**". 


```js
while (condition) {
    // code à éxécuter
    console.log("")
}
```

#### La boucle for
On déclare une boucle for avec mot-réservé **for**.
La bouche est constitué de 3 éléments lors de sa déclaration : 
- **Initialisation** : Déclaration de la variable itérative
- **Condition** : on test si la condition est vraie
- **L'incrémendation / décrémentation** : on vas ajouter/soustraire 1 à notre variable itérative

```js
for (initialisation; condition; incrémentation/décrémentation) {
    // code à éxécuter
}
```
### Les tableaux
Les tableaux sont des variables qui permettent de stocker plusieurs informations de manières structurées.
On déclare un tableau avec les crochets "**[]**".

- [Documentation MDN pour les tableaux ](https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array)

```js
let myArray = ["val1", 00, true, ...]
```

Pour afficher la valeur d'un tableau, il faut appeler l'indice correspondant à la valeur.

```js
// Pour afficher la valeur 09
myArray[1]
```

Il est possible de parcourir les différentes valeurs d'un tableau grâce aux **boucles**.

## Les objets littéraux en javascript
En javascript, il existe le concept objet littéral qui permet d'être entre l'objet et le tableau.
Il se déclare comme une variable. 
Ils fonctionnenent par principe **clé/valeur**.
Ca permet de stoker de nombreuses et diverses informations dans une variable. 

```js
let myLiteralObject = {
    clé : "valeur",
    autreClé : 00,
    encoreAutreClé :true,
    cléTableau : ["valeur1", "valeur2", ...]
}
```

Pour afficher une valeur d'un objet littéral il faut appeler la clé correspondante.

```js
// afficher la valeur : valeur
myLitteralObject.clé

//Autre méthode possible proche de celle des tableaux indicés 
myLitteralObject["clé"]
```

## Le javascript événementiel (intéraction avec le DOM)
Avec Javascript, il est possible de manipuler le HTML et le CSS. 
Ce qui permettra de générer des micro-intéractions.

Quelques exemples :
Récupérer une élément HTML

```html
<div id="monElement">
    ...
    </div>
```

```js
let monElement = document.getElementById("monElement")
//Permet de récupérer l'élément HTML ayant pour id "monElement"
```

Modification du style 

```js
monElement.style.backgroundColor = 'red'
//Permet de changer la couleur d'arrière plan de l'élément en rouge 
```

Ecouteur d'événement 

```js
monElement.addEventListener('click', () =>) {
    // le code à éxécuter lors du clic sur l'élément
}