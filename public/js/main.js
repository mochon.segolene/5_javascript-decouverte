//alert("Bonjour ici Javascript !")

/**
 * Les commentaires
 */
//Commentaire simple
/* Commentaire multi-ligne */ 
/**Commentaire de documentation */

/**
 * Les variables
 * On déclare une variable avec le mot-reservé "var" ou "let" (on privilégiera la déclaration avec "let")
 * On affecte une valeur avec le caractère "="
 * Le type de la variable sera définit dynamiquement en fonction de la valeur 
 */
var firstVariable = "Ma première variable";

console.log(firstVariable)

let varString = "une chaine de caractères"
let varInt = 12
let varBool = true

console.log(varString, varInt, varBool)

let a = 15, 
    b = 23

// Application 
let addition = a + b
let multiplication = a * b

console.log("Résultat de l'addition : ", addition)
console.log("Résultat de la multiplication : ", multiplication)

/**
 *  Les fonctions
 *  */ 
function additionner(valUn, valDeux){
    let resultat = valUn + valDeux

    return resultat
}
console.log(additionner(12,46))
console.log(additionner(a,b))

/**
 * TP : faire une fonction soustraire, diviser et multiplier
 */

function soustraire(valUn, valDeux){
    let resultat = valUn - valDeux

    return resultat
}
console.log(soustraire(5,2))

function diviser(valUn, valDeux){
    let resultat = valUn / valDeux

    return resultat
}
console.log(diviser(10,2))

function multiplier(valUn, valDeux){
    let resultat = valUn * valDeux

    return resultat
}
console.log(multiplier(a,b))

/**
 * Les conditions
 */
if (true) {
    console.log("Ma condition est validée");
}
else {
    console.log("Ma condition n'est pas validée");
}

/**
 * Exemple d'application : travail en fonction de l'âge 
 */

 let age = 45;

 if (age < 18) {
    console.log("Tu es trop jeune pour travailler !")
 }
 else if (age >= 18 && age <= 60){
    console.log("Tu dois travailler !")
 }
 else {
     console.log ("Tu es en retraite !")
 }

 /**
  * Exemple d'application :
  */

  let jour = 6

 switch(jour) {
    case 1:
        console.log("Lundi")
        break
    case 2:
        console.log("Mardi")
        break
    case 3:
        console.log("Mercredi")
        break
    case 4:
        console.log("Jeudi")
        break
    case 5:
        console.log("Vendredi")
        break
    case 6:
        console.log("Samedi")
        break
    case 7:
        console.log("Dimanche")
        break
    default:
        console.log("Ce jour n'existe pas")

}

/**
 * Les boucles
 */

// while (true) {
//    console.log("Bienvenue dans l'infinie et l'au-dela")
//}
// Ci-dessus une boucle infini à ne pas enclencher

let ite = 1

while (ite < 4) {
    console.log("Coucou" + ite)

    //ite = ite+1
    ite ++
}

/**
 * Exemple d'utilisation de la boucle for
 */

 for (let i = 0; i < 4; i++) {
     console.log("Bonjour" + i);
 }

 /**
  * Les tableaux
  */

console.log("\n===== Les tableaux =====")

let tableauDeChaineDeCaractere = ["Mireille", "Marcel", "Martine", "va à la plage"]
let tableauDeNombre = [1, 2, 3, 4]

// L'indice 4 pour ce tableau n'existe pas donc, on a une erreur de type undefined.
console.log("Récupération d'une valeur de mon tableau :", tableauDeChaineDeCaractere[4])

console.log(tableauDeChaineDeCaractere)

tableauDeChaineDeCaractere.push("et c'est cool")

console.log(tableauDeChaineDeCaractere)

console.log(tableauDeChaineDeCaractere[4])

/**
 * Parcours les valeurs du tableau grâce à une boucle 
 */

 for (let i = 0; i< tableauDeChaineDeCaractere.length; i++) {
     console.log("Valeur d'indice" + i + ":" + tableauDeChaineDeCaractere[i])
 }

 /** 
  * TP
  */

  let tableauCouleurs = ["Rouge", "bleu", "vert", "orange", "violet", "jaune"]

  /**
   * Donner la taille du tableau dans la console 
   * Ajouter une couleur au tableau 
   * Donner la nouvelle taille du tableau 
   * Parcourir le tableau de couleur et l'afficher dans la console
   * MAIS sans afficher la 4ème couleur
   */

   console.log("Taille du tableau de couleur  :", tableauCouleurs.length) //OK REUSSI

   tableauCouleurs.push("noir")

   console.log("Nouvelle taille du tableau de couleur :", tableauCouleurs.length)

   for (let i = 0; i< tableauCouleurs.length; i++) {
       if (i != 3) {
        console.log("Couleur pour l'indice" + i + ":" + tableauCouleurs[i])
       }

}

/**
 * Les objets littéraux
 */

console.log("\n===Les objets littéraux====")

let ObjetLitteral = {
    nom: "Joe",
    prenom: "Jane",
    age: 27,
}

// deux façons d'afficher la valeur des objets littéraux
console.log("Affichage de la valeur façon tableau de l'objet littéral :", ObjetLitteral["nom"])
console.log("Affichage de la valeur façon objet de l'objet littéral :", ObjetLitteral.nom)


// Exemple 
console.log("Voici " + ObjetLitteral.nom + " " + ObjetLitteral.prenom + " et elle a " + ObjetLitteral.age + " ans !")


/**
 * Micro TP
 */

console.log("\n====Les objets littéraux====")

let personnes = {
    nom: ["Dupont", "Dupond", "Dupons", "Dupon"],
    prenom: ["Jean", "Pierre", "Paul", "Martin"]

}

for (let i = 0; i < personnes.nom.length; i++) {
    console.log("Nom n°" + i + " : " + personnes.nom[i])
}
for (let i = 0; i < personnes.prenom.length; i++) {
    console.log("Prénom n°" + i + " : " + personnes.prenom[i])
}

/**
 * 
 * Intéraction avec le DOM HTML
 */

console.log("\n====Intéraction avec le DOM HTML====")

let btnEvent = document.getElementById("btn-event")

console.log(btnEvent)
btnEvent.style.backgroundColor = 'red'

// on met un écouteur d'événement sur le bouton
// Ancienne version

btnEvent.addEventListener('click', function(){
    console.log("Super j'ai réussi à cliquer")
})

// Nouvelle version (ES15) : écouter sur le survol

btnEvent.addEventListener('mouseover', ()=> {
    console.log("Je survol le bouton")
})

/**
 * Micro TP
 * - Créer un bouton qui lorsque l'on clic dessus change la couleur du body
 * - faire en sorte qu'à chaque nouveau clic, ce soit une nouvelle couleur 
 */

let btnColor = document.getElementById("btn-color")

console.log(btnColor)

function bgColor() {
    let red = Math.floor((Math.random() * 255))
    let green = Math.floor((Math.random() * 255))
    let blue = Math.floor((Math.random() * 255))

    return "rgb(" + red + "," + green + "," + blue + ")";
}

btnColor.addEventListener('click', () => {
   // console.log(body.style.color = 'red')
    document.body.style.backgroundColor = bgColor()
})


